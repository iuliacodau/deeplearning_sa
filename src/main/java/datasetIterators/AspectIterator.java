package datasetIterators;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by codaui on 04.07.2017.
 */
public class AspectIterator implements DataSetIterator{
    private final WordVectors wordVectors;
    private final int batchSize;
    private final int vectorSize;
    private final int truncateLength;

    private int cursor = 0;
    private final File[] aspectFiles;
    private final TokenizerFactory tokenizerFactory;
    private HashMap<String, Integer> aspectMap;

    public AspectIterator(String dataDirectory, WordVectors wordVectors, int batchSize, int truncateLength, boolean train) throws IOException {
        this.batchSize = batchSize;
        this.vectorSize = wordVectors.getWordVector(wordVectors.vocab().wordAtIndex(0)).length;


        File p = new File(FilenameUtils.concat(dataDirectory, (train ? "Train" : "Test") + "/Aspects") + "/");
        aspectFiles = p.listFiles();

        this.wordVectors = wordVectors;
        this.truncateLength = truncateLength;

        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());

        createAspectMap();
    }

    private void createAspectMap() {
        this.aspectMap = new HashMap<>();
        this.aspectMap.put("food", 0);
        this.aspectMap.put("drinks", 1);
        this.aspectMap.put("service", 2);



    }

    @Override
    public DataSet next(int num) {
        if (cursor >= aspectFiles.length) throw new NoSuchElementException();
        try{
            return nextDataSet(num);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    private DataSet nextDataSet(int num) throws IOException {
        List<String> reviews = new ArrayList<>(num);

        int[] aspect = new int[num];



        for( int i=0; i<num && cursor<totalExamples(); i++ ){
            String review = FileUtils.readFileToString(aspectFiles[cursor]);
            reviews.add(review);
            List<String> tokens = tokenizerFactory.create(review).getTokens();
            String entity = tokens.get(tokens.size() - 1).toLowerCase();
           // String attribute = tokens.get(tokens.size() - 1).toLowerCase();
           // StringBuilder sb = new StringBuilder();
           // sb.append(entity);
           // sb.append(" ");
           // sb.append(attribute);
           /* if ("ambience".equals(entity) || "location".equals(entity)) {
                entity = "restaurant";
                String checker = aspectEntry.getKey();
            }*/


            for (Map.Entry<String, Integer> aspectEntry : aspectMap.entrySet()) {
                if (aspectEntry.getKey().equals(entity)) {
                    aspect[i] = aspectEntry.getValue();
                }
            }

            cursor++;
        }
        List<List<String>> allTokens = new ArrayList<>(reviews.size());
        List<Pair<String, String>> listOfAspects = new ArrayList<>(reviews.size());
        List<INDArray> allAspectVectors = new ArrayList<>();
        int maxLength = 0;
        for(String s : reviews){
            List<String> tokens = tokenizerFactory.create(s).getTokens();
            List<String> tokensFiltered = new ArrayList<>();
            int indexForAspect = tokens.size()-2;
            String entity = tokens.get(tokens.size()-1).toLowerCase();
          //  String attribute = tokens.get(tokens.size()-1).toLowerCase();
          //  listOfAspects.add(new ImmutablePair<>(entity, attribute));
            tokens.remove(indexForAspect);
            tokens.remove(entity);
         //   tokens.remove(attribute);
           /* if (wordVectors.hasWord(entity) && wordVectors.hasWord(attribute)) {
                INDArray entityVector = wordVectors.getWordVectorMatrix(entity);
                INDArray attributeVector = wordVectors.getWordVectorMatrix(attribute);
                INDArray aspectVector = (entityVector.add(attributeVector)).mul(0.5);
                allAspectVectors.add(aspectVector);
            }
            else {
                allAspectVectors.add(Nd4j.create(300));
            }*/
            for(String t : tokens ){
                if(wordVectors.hasWord(t)) tokensFiltered.add(t);
            }
            allTokens.add(tokensFiltered);
            maxLength = Math.max(maxLength,tokensFiltered.size());
        }
        if(maxLength > truncateLength) maxLength = truncateLength;
        INDArray features = Nd4j.create(reviews.size(), vectorSize, maxLength);
        INDArray labels = Nd4j.create(reviews.size(), 3, maxLength);
        INDArray featuresMask = Nd4j.zeros(reviews.size(), maxLength);
        INDArray labelsMask = Nd4j.zeros(reviews.size(), maxLength);

        int[] temp = new int[2];
        for( int i=0; i<reviews.size(); i++ ){
            List<String> tokens = allTokens.get(i);
            temp[0] = i;
            for( int j=0; j<tokens.size() && j<maxLength; j++ ){
                String token = tokens.get(j);
                INDArray vector = wordVectors.getWordVectorMatrix(token);
                features.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.point(j)}, vector);
                temp[1] = j;
                featuresMask.putScalar(temp, 1.0);
            }
            int idx = aspect[i];
            int lastIdx = Math.min(tokens.size(),maxLength);
            labels.putScalar(new int[]{i,idx,lastIdx-1},1.0);
            labelsMask.putScalar(new int[]{i,lastIdx-1},1.0);
        }
        return new DataSet(features,labels,featuresMask,labelsMask);
    }

    @Override
    public int totalExamples() {
        return aspectFiles.length;
    }

    @Override
    public int inputColumns() {
        return vectorSize;
    }

    @Override
    public int totalOutcomes() {
        return 5;
    }

    @Override
    public void reset() {
        cursor = 0;
    }

    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public int cursor() {
        return cursor;
    }

    @Override
    public int numExamples() {
        return totalExamples();
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> getLabels() {
        List<String> labelsList = new ArrayList();
        labelsList.addAll(aspectMap.keySet());
        return labelsList;
    }

    @Override
    public boolean hasNext() {
        return cursor < numExamples();
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }

    @Override
    public void remove() {

    }
    @Override
    public  DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public INDArray loadFeaturesFromString(String reviewContents, int maxLength){
        List<String> tokens = tokenizerFactory.create(reviewContents).getTokens();
        List<String> tokensFiltered = new ArrayList<>();
        for(String t : tokens ){
            if(wordVectors.hasWord(t)) tokensFiltered.add(t);
        }
        int outputLength = Math.max(maxLength,tokensFiltered.size());

        INDArray features = Nd4j.create(1, vectorSize, outputLength);

        for( int j=0; j<tokens.size() && j<maxLength; j++ ){
            String token = tokens.get(j);
            INDArray vector = wordVectors.getWordVectorMatrix(token);
            features.put(new INDArrayIndex[]{NDArrayIndex.point(0), NDArrayIndex.all(), NDArrayIndex.point(j)}, vector);
        }
        return features;
    }
}
