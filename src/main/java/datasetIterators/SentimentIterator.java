package datasetIterators;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.text.sentenceiterator.CollectionSentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Asus on 7/1/2017.
 */
public class SentimentIterator implements DataSetIterator {

    public static final String PATH_TO_LOAD = "src/main/resources/aspectsNetwork3.net";
    private static MultiLayerNetwork net;
    public static final String WORD_VECTORS_PATH = "src/main/resources/GoogleNews-vectors-negative300.bin.gz";
    private static WordVectors wordVectors;
    private final int batchSize;
    private final int vectorSize;
    private final int truncateLength;

    private int cursor = 0;
    private final File[] positiveFiles;
    //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           private final File[] neutralFiles;
    private final File[] negativeFiles;
    private static TokenizerFactory tokenizerFactory;
    private HashMap<String, Integer> aspectMap;

    public SentimentIterator(String dataDirectory, WordVectors wordVectors, int batchSize, int truncateLength, boolean train) throws IOException {
        this.batchSize = batchSize;
        this.vectorSize = wordVectors.getWordVector(wordVectors.vocab().wordAtIndex(0)).length;


        File p = new File(FilenameUtils.concat(dataDirectory, (train ? "Train" : "Test") + "/positives") + "/");
        File n = new File(FilenameUtils.concat(dataDirectory, (train ? "Train" : "Test") + "/negatives") + "/");
        positiveFiles = p.listFiles();
        negativeFiles = n.listFiles();

        this.wordVectors = wordVectors;
        this.truncateLength = truncateLength;

        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        createAspectMap();
    }

    private void createAspectMap() {
        this.aspectMap = new HashMap<>();
        this.aspectMap.put("food", 0);
        this.aspectMap.put("drinks", 1);
        this.aspectMap.put("service", 2);

    }

    @Override
    public DataSet next(int num) {
        if (cursor >= positiveFiles.length + negativeFiles.length) throw new NoSuchElementException();
        try{
            return nextDataSet(num);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    private DataSet nextDataSet(int num) throws IOException {
        //First: load reviews to String. Alternate positive and negative reviews
        List<String> reviews = new ArrayList<>(num);
        List<Integer> positive = new ArrayList<>(num);
        for( int i=0; i<num && cursor<totalExamples(); i++ ){
            if(cursor % 2 == 0){
                //Load positive review
                int posReviewNumber = cursor / 2;
                String review = FileUtils.readFileToString(positiveFiles[posReviewNumber]);
                reviews.add(review);
                positive.add(i, 1);
            } if (cursor % 2 == 1) {
                //Load negative review
                int negReviewNumber = cursor / 2;
                String review = FileUtils.readFileToString(negativeFiles[negReviewNumber]);
                reviews.add(review);
                positive.add(i, 0);
            }
            cursor++;
        }

       // Random rndm = new Random();
       // Collections.shuffle(reviews, rndm);
       // Collections.shuffle(positive, rndm);



        MultiLayerNetwork restored = ModelSerializer.restoreMultiLayerNetwork(PATH_TO_LOAD);
        List<List<String>> allTokens = new ArrayList<>(reviews.size());
        List<INDArray> listOfAspectVectors = new ArrayList<>(reviews.size());
        int maxLength = 0;
        for(String s : reviews){
            List<String> tokens = tokenizerFactory.create(s).getTokens();
            List<String> tokensFiltered = new ArrayList<>();






            net = ModelSerializer.restoreMultiLayerNetwork("src/main/resources/aspectsNetwork3.net");
            File categories = new File("src/main/resources/categories.txt");
            DataSet testNews = prepareTestData(s);
            INDArray fet = testNews.getFeatureMatrix();
            INDArray predicted = net.output(fet, false);
            int arrsiz[] = predicted.shape();

            double max = 0;
            int pos = 0;
            for (int i = 0; i < arrsiz[1]; i++) {
                System.out.println((double) predicted.getColumn(i).sumNumber());
                if (max < (double) predicted.getColumn(i).sumNumber()) {
                    max = (double) predicted.getColumn(i).sumNumber();
                    pos = i;
                }
            }

            try (BufferedReader brCategories = new BufferedReader(new FileReader(categories))) {
                String temp = "";
                List<String> labels = new ArrayList<>();
                while ((temp = brCategories.readLine()) != null) {
                    labels.add(temp);
                }
                brCategories.close();
                System.out.println(labels.get(pos).split(",")[1]);
            } catch (Exception e) {
                System.out.println("File Exception : " + e.getMessage());
            }

            System.out.println("Highest value label " + pos);
             if (pos == 0) {
                 listOfAspectVectors.add(wordVectors.getWordVectorMatrix("food"));
             }
            if (pos == 1) {
                listOfAspectVectors.add(wordVectors.getWordVectorMatrix("drinks"));
            }
            if (pos == 2) {
                listOfAspectVectors.add(wordVectors.getWordVectorMatrix("service"));
            }



/*
            int indexForAspect = tokens.size()-3;
            String entity = tokens.get(tokens.size()-2).toLowerCase();
            String attribute = tokens.get(tokens.size()-1).toLowerCase();


            tokens.remove(indexForAspect);
            tokens.remove(entity);
            tokens.remove(attribute);
*/


            int indexForAspect = tokens.size()-2;
            String entity = tokens.get(tokens.size()-1).toLowerCase();



            tokens.remove(indexForAspect);
            tokens.remove(entity);


            for(String t : tokens ){
                if(wordVectors.hasWord(t)) tokensFiltered.add(t);
            }
            allTokens.add(tokensFiltered);
            maxLength = Math.max(maxLength,tokensFiltered.size());
        }
        if(maxLength > truncateLength) maxLength = truncateLength;
        INDArray features = Nd4j.create(reviews.size(), 2*vectorSize, maxLength);
        INDArray labels = Nd4j.create(reviews.size(), 2, maxLength);
        INDArray featuresMask = Nd4j.zeros(reviews.size(), maxLength);
        INDArray labelsMask = Nd4j.zeros(reviews.size(), maxLength);

        int[] temp = new int[2];
        for( int i=0; i<reviews.size(); i++ ){
            List<String> tokens = allTokens.get(i);
            temp[0] = i;
            INDArray aVector = listOfAspectVectors.get(i);
            for( int j=0; j<tokens.size() && j<maxLength; j++ ){
                String token = tokens.get(j);


                INDArray vector = wordVectors.getWordVectorMatrix(token);



                INDArray wordAndAspectVector = Nd4j.concat(0,vector, aVector);
                features.put(new INDArrayIndex[]{NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.point(j)}, wordAndAspectVector);
                temp[1] = j;
                featuresMask.putScalar(temp, 1.0);  //Word is present (not padding) for this example + time step -> 1.0 in features mask
            }

            int idx = positive.get(i);
            int lastIdx = Math.min(tokens.size(),maxLength);
            labels.putScalar(new int[]{i,idx,lastIdx-1},1.0);   //Set label: [0,1] for negative, [1,0] for positive
            labelsMask.putScalar(new int[]{i,lastIdx-1},1.0);   //Specify that an output exists at the final time step for this example
        }

        return new DataSet(features,labels,featuresMask,labelsMask);
    }

    @Override
    public int totalExamples() {
        return positiveFiles.length + negativeFiles.length;
    }

    @Override
    public int inputColumns() {
        return vectorSize;
    }

    @Override
    public int totalOutcomes() {
        return 2;
    }

    @Override
    public void reset() {
        cursor = 0;
    }

    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public int cursor() {
        return cursor;
    }

    @Override
    public int numExamples() {
        return totalExamples();
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> getLabels() {
        return Arrays.asList("positive","negative");
    }

    @Override
    public boolean hasNext() {
        return cursor < numExamples();
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }

    @Override
    public void remove() {

    }
    @Override
    public  DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    /** Convenience method for loading review to String */
    public String loadReviewToString(int index) throws IOException{
        File f;
        if(index%2 == 0) f = positiveFiles[index/2];
        else f = negativeFiles[index/2];
        return FileUtils.readFileToString(f);
    }

    /** Convenience method to get label for review */
    public boolean isPositiveReview(int index){
        return index%2 == 0;
    }

    /**
     * Used post training to load a review from a file to a features INDArray that can be passed to the network output method
     *
     * @param file      File to load the review from
     * @param maxLength Maximum length (if review is longer than this: truncate to maxLength). Use Integer.MAX_VALUE to not nruncate
     * @return          Features array
     * @throws IOException If file cannot be read
     */
    public INDArray loadFeaturesFromFile(File file, int maxLength) throws IOException {
        String review = FileUtils.readFileToString(file);
        return loadFeaturesFromString(review, maxLength);
    }

    /**
     * Used post training to convert a String to a features INDArray that can be passed to the network output method
     *
     * @param reviewContents Contents of the review to vectorize
     * @param maxLength Maximum length (if review is longer than this: truncate to maxLength). Use Integer.MAX_VALUE to not nruncate
     * @return Features array for the given input String
     */
    public INDArray loadFeaturesFromString(String reviewContents, int maxLength){
        List<String> tokens = tokenizerFactory.create(reviewContents).getTokens();
        List<String> tokensFiltered = new ArrayList<>();
        for(String t : tokens ){
            if(wordVectors.hasWord(t)) tokensFiltered.add(t);
        }
        int outputLength = Math.max(maxLength,tokensFiltered.size());

        INDArray features = Nd4j.create(1, vectorSize, outputLength);

        for( int j=0; j<tokens.size() && j<maxLength; j++ ){
            String token = tokens.get(j);
            INDArray vector = wordVectors.getWordVectorMatrix(token);
            features.put(new INDArrayIndex[]{NDArrayIndex.point(0), NDArrayIndex.all(), NDArrayIndex.point(j)}, vector);
        }

        return features;
    }

    private static DataSet prepareTestData(String i_news) {

        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        List<String> news = new ArrayList<>(1);
        int[] category = new int[1];
        int currCategory = 0;
        news.add(i_news);

        List<List<String>> allTokens = new ArrayList<>(news.size());
        int maxLength = 0;
        for (String s : news) {
            List<String> tokens = tokenizerFactory.create(s).getTokens();
            List<String> tokensFiltered = new ArrayList<>();
            for (String t : tokens) {
                if (wordVectors.hasWord(t)) tokensFiltered.add(t);
            }
            allTokens.add(tokensFiltered);
            maxLength = Math.max(maxLength, tokensFiltered.size());
        }

        INDArray features = Nd4j.create(news.size(), 300, maxLength);
        INDArray labels = Nd4j.create(news.size(), 3, maxLength);
        INDArray featuresMask = Nd4j.zeros(news.size(), maxLength);
        INDArray labelsMask = Nd4j.zeros(news.size(), maxLength);

        int[] temp = new int[2];
        for (int i = 0; i < news.size(); i++) {
            List<String> tokens = allTokens.get(i);
            temp[0] = i;
            for (int j = 0; j < tokens.size() && j < maxLength; j++) {
                String token = tokens.get(j);
                INDArray vector = wordVectors.getWordVectorMatrix(token);
                features.put(new INDArrayIndex[]{NDArrayIndex.point(i),
                                NDArrayIndex.all(),
                                NDArrayIndex.point(j)},
                        vector);

                temp[1] = j;
                featuresMask.putScalar(temp, 1.0);
            }
            int idx = category[i];
            int lastIdx = Math.min(tokens.size(), maxLength);
            labels.putScalar(new int[]{i, idx, lastIdx - 1}, 1.0);
            labelsMask.putScalar(new int[]{i, lastIdx - 1}, 1.0);
        }

        DataSet ds = new DataSet(features, labels, featuresMask, labelsMask);
        return ds;
    }
}