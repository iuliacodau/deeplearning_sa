
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Reviews_QNAME = new QName("", "Reviews");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReviewsType }
     * 
     */
    public ReviewsType createReviewsType() {
        return new ReviewsType();
    }

    /**
     * Create an instance of {@link SentenceType }
     * 
     */
    public SentenceType createSentenceType() {
        return new SentenceType();
    }

    /**
     * Create an instance of {@link OpinionsType }
     * 
     */
    public OpinionsType createOpinionsType() {
        return new OpinionsType();
    }

    /**
     * Create an instance of {@link SentencesType }
     * 
     */
    public SentencesType createSentencesType() {
        return new SentencesType();
    }

    /**
     * Create an instance of {@link OpinionType }
     * 
     */
    public OpinionType createOpinionType() {
        return new OpinionType();
    }

    /**
     * Create an instance of {@link ReviewType }
     * 
     */
    public ReviewType createReviewType() {
        return new ReviewType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReviewsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Reviews")
    public JAXBElement<ReviewsType> createReviews(ReviewsType value) {
        return new JAXBElement<ReviewsType>(_Reviews_QNAME, ReviewsType.class, null, value);
    }

}
