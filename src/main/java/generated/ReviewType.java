
package generated;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReviewType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReviewType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sentences" type="{}SentencesType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReviewType", propOrder = {
    "sentences"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class ReviewType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected SentencesType sentences;

    /**
     * Gets the value of the sentences property.
     * 
     * @return
     *     possible object is
     *     {@link SentencesType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public SentencesType getSentences() {
        return sentences;
    }

    /**
     * Sets the value of the sentences property.
     * 
     * @param value
     *     allowed object is
     *     {@link SentencesType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setSentences(SentencesType value) {
        this.sentences = value;
    }

}
