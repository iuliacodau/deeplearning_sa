
package generated;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OpinionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpinionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="to" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="from" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="polarity" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="category" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="target" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpinionType")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class OpinionType {

    @XmlAttribute(name = "to")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BigInteger to;
    @XmlAttribute(name = "from")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BigInteger from;
    @XmlAttribute(name = "polarity")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String polarity;
    @XmlAttribute(name = "category")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String category;
    @XmlAttribute(name = "target")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String target;

    /**
     * Gets the value of the to property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public BigInteger getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setTo(BigInteger value) {
        this.to = value;
    }

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public BigInteger getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFrom(BigInteger value) {
        this.from = value;
    }

    /**
     * Gets the value of the polarity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getPolarity() {
        return polarity;
    }

    /**
     * Sets the value of the polarity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setPolarity(String value) {
        this.polarity = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the target property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getTarget() {
        return target;
    }

    /**
     * Sets the value of the target property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setTarget(String value) {
        this.target = value;
    }

}
