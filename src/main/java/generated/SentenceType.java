
package generated;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SentenceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SentenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="text" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Opinions" type="{}OpinionsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SentenceType", propOrder = {
    "text",
    "opinions"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class SentenceType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String text;
    @XmlElement(name = "Opinions", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected OpinionsType opinions;

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the opinions property.
     * 
     * @return
     *     possible object is
     *     {@link OpinionsType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public OpinionsType getOpinions() {
        return opinions;
    }

    /**
     * Sets the value of the opinions property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpinionsType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-07-01T02:35:11+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setOpinions(OpinionsType value) {
        this.opinions = value;
    }

}
