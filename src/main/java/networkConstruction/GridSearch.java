package networkConstruction;


import datasetIterators.SentimentIterator;

import org.deeplearning4j.arbiter.DL4JConfiguration;
import org.deeplearning4j.arbiter.MultiLayerSpace;
import org.deeplearning4j.arbiter.data.DataSetIteratorProvider;
import org.deeplearning4j.arbiter.layers.*;
import org.deeplearning4j.arbiter.optimize.api.CandidateGenerator;
import org.deeplearning4j.arbiter.optimize.api.OptimizationResult;
import org.deeplearning4j.arbiter.optimize.api.ParameterSpace;
import org.deeplearning4j.arbiter.optimize.api.data.DataProvider;
import org.deeplearning4j.arbiter.optimize.api.saving.ResultReference;
import org.deeplearning4j.arbiter.optimize.api.saving.ResultSaver;
import org.deeplearning4j.arbiter.optimize.api.score.ScoreFunction;
import org.deeplearning4j.arbiter.optimize.api.termination.MaxTimeCondition;
import org.deeplearning4j.arbiter.optimize.api.termination.TerminationCondition;
import org.deeplearning4j.arbiter.optimize.candidategenerator.GridSearchCandidateGenerator;
import org.deeplearning4j.arbiter.optimize.candidategenerator.RandomSearchGenerator;
import org.deeplearning4j.arbiter.optimize.config.OptimizationConfiguration;
import org.deeplearning4j.arbiter.optimize.parameter.continuous.ContinuousParameterSpace;
import org.deeplearning4j.arbiter.optimize.parameter.integer.IntegerParameterSpace;
import org.deeplearning4j.arbiter.optimize.runner.IOptimizationRunner;
import org.deeplearning4j.arbiter.optimize.runner.LocalOptimizationRunner;
import org.deeplearning4j.arbiter.saver.local.multilayer.LocalMultiLayerNetworkSaver;
import org.deeplearning4j.arbiter.scoring.multilayer.TestSetAccuracyScoreFunction;
import org.deeplearning4j.arbiter.task.MultiLayerNetworkTaskCreator;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;

import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Automatic search for the hyperparameters of the network
 * The implementation is based on the paper by
 *
 */
public class GridSearch {

    public static final String DATA_PATH = "src/main/resources/DataWithAspects/";
    public static final String WORD_VECTORS_PATH = "src/main/resources/GoogleNews-vectors-negative300.bin.gz";

    public static void main(String[] args) throws Exception {
        ParameterSpace<Double> learningRateHyperparam = new ContinuousParameterSpace(0.001, 0.1);
        ParameterSpace<Integer> layerSizeHyperparam = new IntegerParameterSpace(120, 300);
        ParameterSpace<Double> l2regularizationHyperparam = new ContinuousParameterSpace(0.000001, 0.001);

        MultiLayerSpace hyperparameterSpace = new MultiLayerSpace.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .iterations(1)
                .weightInit(WeightInit.XAVIER)
                .regularization(true)
                .l2(l2regularizationHyperparam)
                .learningRate(learningRateHyperparam)
                .addLayer(new GravesBidirectionalLSTMLayerSpace.Builder()
                        .nIn(600)
                        .activation("tanh")
                        .nOut(layerSizeHyperparam)
                        .build())
                .addLayer(new GravesBidirectionalLSTMLayerSpace.Builder()
                        .nIn(layerSizeHyperparam)
                        .nOut(layerSizeHyperparam)
                        .activation("tanh")
                        .build())
                .addLayer(new RnnOutputLayerSpace.Builder()
                        .nIn(layerSizeHyperparam)
                                .activation("softmax")
                                .lossFunction(LossFunctions.LossFunction.MCXENT)
                        .nOut(2)
                        .build()
                        )
                .pretrain(false).backprop(true).build();
        CandidateGenerator<DL4JConfiguration> candidateGenerator = new RandomSearchGenerator<>(hyperparameterSpace);
        //Alternatively: new GridSearchCandidateGenerator<>(hyperparameterSpace, 5, GridSearchCandidateGenerator.Mode.RandomOrder);
        new GridSearchCandidateGenerator<>(hyperparameterSpace, 5, GridSearchCandidateGenerator.Mode.RandomOrder);
        int nTrainEpochs = 5;

        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(WORD_VECTORS_PATH));
        SentimentIterator train = new SentimentIterator(DATA_PATH, wordVectors, 5, 256, true);;
        SentimentIterator test = new SentimentIterator(DATA_PATH, wordVectors, 5, 256, false);
        DataProvider<DataSetIterator> dataProvider = new DataSetIteratorProvider(train, test);

        // (c) How we are going to save the models that are generated and tested?
        //     In this example, let's save them to disk the working directory
        //     This will result in examples being saved to arbiterExample/0/, arbiterExample/1/, arbiterExample/2/, ...
        String baseSaveDirectory = "arbiterResults/";
        File f = new File(baseSaveDirectory);
        if (f.exists()) f.delete();
        f.mkdir();
        ResultSaver<DL4JConfiguration, MultiLayerNetwork, Object> modelSaver = new LocalMultiLayerNetworkSaver<>(baseSaveDirectory);

        // (d) What are we actually trying to optimize?
        //     In this example, let's use classification accuracy on the test set
        ScoreFunction<MultiLayerNetwork, DataSetIterator> scoreFunction = new TestSetAccuracyScoreFunction();

        // (e) When should we stop searching? Specify this with termination conditions
        //     For this example, we are stopping the search at 15 minutes or 20 candidates - whichever comes first
        TerminationCondition[] terminationConditions = {new MaxTimeCondition(5, TimeUnit.HOURS)};


        //Given these configuration options, let's put them all together:
        OptimizationConfiguration<DL4JConfiguration, MultiLayerNetwork, DataSetIterator, Object> configuration
                = new OptimizationConfiguration.Builder<DL4JConfiguration, MultiLayerNetwork, DataSetIterator, Object>()
                .candidateGenerator(candidateGenerator)
                .dataProvider(dataProvider)
                .modelSaver(modelSaver)
                .scoreFunction(scoreFunction)
                .terminationConditions(terminationConditions)
                .build();

        //And set up execution locally on this machine:
        IOptimizationRunner<DL4JConfiguration, MultiLayerNetwork, Object> runner
                = new LocalOptimizationRunner<>(configuration, new MultiLayerNetworkTaskCreator<>());

        runner.execute();


        //Print out some basic stats regarding the optimization procedure
        StringBuilder sb = new StringBuilder();
        sb.append("Best score: ").append(runner.bestScore()).append("\n")
                .append("Index of model with best score: ").append(runner.bestScoreCandidateIndex()).append("\n")
                .append("Number of configurations evaluated: ").append(runner.numCandidatesCompleted()).append("\n");
        System.out.println(sb.toString());


        //Get all results, and print out details of the best result:
        int indexOfBestResult = runner.bestScoreCandidateIndex();
        List<ResultReference<DL4JConfiguration, MultiLayerNetwork, Object>> allResults = runner.getResults();

        OptimizationResult<DL4JConfiguration, MultiLayerNetwork, Object> bestResult = allResults.get(indexOfBestResult).getResult();
        MultiLayerNetwork bestModel = bestResult.getResult();

        System.out.println("\n\nConfiguration of best model:\n");
        System.out.println(bestModel.getLayerWiseConfigurations().toJson());
    }
}
