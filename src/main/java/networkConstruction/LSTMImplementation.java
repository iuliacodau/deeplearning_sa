package networkConstruction;

import datasetIterators.SentimentIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesBidirectionalLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by Asus on 7/1/2017.
 */
public class LSTMImplementation {



    //public static final String DATA_PATH = "C:/Folder/Lic/Data/DataWithAspects/";
    public static final String DATA_PATH = "src/main/resources/DataWithAspects/";
    public static final String WORD_VECTORS_PATH = "C:/Folder/Lic/GoogleNews-vectors-negative300.bin.gz";
    private static MultiLayerNetwork net;
    private static TokenizerFactory tokenizerFactory;


    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        System.out.println("Starting at hour: " + startTime);
        if(WORD_VECTORS_PATH.startsWith("/PATH/TO/YOUR/VECTORS/")){
            throw new RuntimeException("Please set the WORD_VECTORS_PATH before running this example");
        }

/*
        int batchSize = 5;     //Number of examples in each minibatch
        int vectorSize = 600;   //Size of the word vectors. 300 in the Google News model
        int nEpochs = 1;        //Number of epochs (full passes of training data) to train on
        int truncateReviewsToLength = 256;  //Truncate reviews with length (# words) greater than this

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                //.updater(Updater.ADAM).adamMeanDecay(0.9).adamVarDecay(0.999)
                .updater(Updater.ADAGRAD)
                .regularization(true).l2(1e-5)
                .weightInit(WeightInit.XAVIER)
                //.gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue).gradientNormalizationThreshold(1.0)
                .learningRate(0.005)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .list()
                .layer(0, new GravesBidirectionalLSTM.Builder().nIn(vectorSize).nOut(256)
                        .activation(Activation.TANH).build())
                .layer(1, new GravesBidirectionalLSTM.Builder().nIn(256).nOut(256)
                        .activation(Activation.TANH).build())
                .layer(2, new RnnOutputLayer.Builder().activation(Activation.SOFTMAX)
                        .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(256).nOut(2).build())
                .pretrain(false).backprop(true).build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(1));

        //DataSetIterators for training and testing respectively
        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(WORD_VECTORS_PATH));
        SentimentIterator train = new SentimentIterator(DATA_PATH, wordVectors, batchSize, truncateReviewsToLength, true);
        SentimentIterator test = new SentimentIterator(DATA_PATH, wordVectors, batchSize, truncateReviewsToLength, false);
        System.out.println("Starting training");
        for (int i = 0; i < nEpochs; i++) {
            net.fit(train);
            train.reset();
            System.out.println("Epoch " + i + " complete. Starting evaluation:");

            Evaluation evaluation = new Evaluation();
            while (test.hasNext()) {
                DataSet t = test.next();
                INDArray features = t.getFeatureMatrix();
                INDArray lables = t.getLabels();
                INDArray inMask = t.getFeaturesMaskArray();
                INDArray outMask = t.getLabelsMaskArray();
                INDArray predicted = net.output(features, false, inMask, outMask);

                evaluation.evalTimeSeries(lables, predicted, outMask);
            }
            test.reset();

            System.out.println(evaluation.stats());

        }

        File locationToSave = new File("src/main/resources/sentimentNetwork.net");
        ModelSerializer.writeModel(net, locationToSave, true);
*/

        net = ModelSerializer.restoreMultiLayerNetwork("src/main/resources/sentimentNetwork.net");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter sentence:");
        String review=(scanner.nextLine());
        System.out.println("The aspect is:");
        String aspect=(scanner.nextLine());

        File categories = new File("src/main/resources/polarities.txt");
        DataSet testNews = prepareTestData(review, aspect);
        INDArray fet = testNews.getFeatureMatrix();
        INDArray predicted = net.output(fet, false);
        int arrsiz[] = predicted.shape();

        double max = 0;
        int pos = 0;
        for (int i = 0; i < arrsiz[1]; i++) {
            System.out.println((double) predicted.getColumn(i).sumNumber());
            if (max < (double) predicted.getColumn(i).sumNumber()) {
                max = (double) predicted.getColumn(i).sumNumber();
                pos = i;
            }
        }

        try (BufferedReader brCategories = new BufferedReader(new FileReader(categories))) {
            String temp = "";
            List<String> labels = new ArrayList<>();
            while ((temp = brCategories.readLine()) != null) {
                labels.add(temp);
            }
            brCategories.close();
            System.out.println(labels.get(pos).split(",")[1]);
        } catch (Exception e) {
            System.out.println("File Exception : " + e.getMessage());
        }

        System.out.println("The polarity of the sentence " + pos);



        long endTime = System.currentTimeMillis();
        System.out.println("Execution took " + TimeUnit.MILLISECONDS.toMinutes(endTime-startTime) + " minutes");














        //After training: load a single example and generate predictions
/*
        File firstPositiveReviewFile = new File(FilenameUtils.concat(DATA_PATH, "test/positives/3.txt"));
        String firstPositiveReview = FileUtils.readFileToString(firstPositiveReviewFile);
        //Scanner scanner = new Scanner(System.in);
        //System.out.println("Enter sentence:");
        //String firstPositiveReview=(scanner.nextLine());

        INDArray features = test.loadFeaturesFromString(firstPositiveReview, truncateReviewsToLength);
        INDArray networkOutput = net.output(features);
        int timeSeriesLength = networkOutput.size(2);
        INDArray probabilitiesAtLastWord = networkOutput.get(NDArrayIndex.point(0), NDArrayIndex.all(), NDArrayIndex.point(timeSeriesLength - 1));

        System.out.println("\n\n-------------------------------");
        System.out.println("First positive review: \n" + firstPositiveReview);
        System.out.println("\n\nProbabilities at last time step:");
        System.out.println("p(positive): " + probabilitiesAtLastWord.getDouble(1));
        System.out.println("p(negative): " + probabilitiesAtLastWord.getDouble(0));

        System.out.println("----- Example complete -----");

        System.out.println("Finishing at hour: " + System.currentTimeMillis());
*/
    }


    private static DataSet prepareTestData(String i_news, String aspect) {
        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(WORD_VECTORS_PATH));
        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        List<String> news = new ArrayList<>(1);
        int[] category = new int[1];
        int currCategory = 0;
        news.add(i_news);

        List<List<String>> allTokens = new ArrayList<>(news.size());
        int maxLength = 0;
        for (String s : news) {
            List<String> tokens = tokenizerFactory.create(s).getTokens();
            List<String> tokensFiltered = new ArrayList<>();
            for (String t : tokens) {
                if (wordVectors.hasWord(t)) tokensFiltered.add(t);
            }
            allTokens.add(tokensFiltered);
            maxLength = Math.max(maxLength, tokensFiltered.size());
        }

        INDArray features = Nd4j.create(news.size(), 600, maxLength);
        INDArray labels = Nd4j.create(news.size(), 2, maxLength);
        INDArray featuresMask = Nd4j.zeros(news.size(), maxLength);
        INDArray labelsMask = Nd4j.zeros(news.size(), maxLength);

        int[] temp = new int[2];
        for (int i = 0; i < news.size(); i++) {
            List<String> tokens = allTokens.get(i);
            temp[0] = i;
            for (int j = 0; j < tokens.size() && j < maxLength; j++) {
                String token = tokens.get(j);
                INDArray vector = wordVectors.getWordVectorMatrix(token);
                INDArray aspectVector = wordVectors.getWordVectorMatrix(aspect);
                INDArray finalV = Nd4j.concat(0,vector, aspectVector);
                features.put(new INDArrayIndex[]{NDArrayIndex.point(i),
                                NDArrayIndex.all(),
                                NDArrayIndex.point(j)},
                        finalV);

                temp[1] = j;
                featuresMask.putScalar(temp, 1.0);
            }
            int idx = category[i];
            int lastIdx = Math.min(tokens.size(), maxLength);
            labels.putScalar(new int[]{i, idx, lastIdx - 1}, 1.0);
            labelsMask.putScalar(new int[]{i, lastIdx - 1}, 1.0);
        }

        DataSet ds = new DataSet(features, labels, featuresMask, labelsMask);
        return ds;
    }


}