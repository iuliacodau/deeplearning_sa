package shallowNetworks;

import datasetIterators.AspectIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesBidirectionalLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.text.sentenceiterator.SynchronizedSentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by codaui on 04.07.2017.
 */
public class LSTMAspects {

    //public static final String DATA_PATH = "C:/Folder/Lic/Data/DataWithAspects/";
    public static final String DATA_PATH = "src/main/resources/DataWithAspects/";
    public static final String WORD_VECTORS_PATH = "src/main/resources/GoogleNews-vectors-negative300.bin.gz";
    public static final String STATS_FILE = "src/main/resources/stats_file.txt";
    private static MultiLayerNetwork net;
    private static TokenizerFactory tokenizerFactory;


    public static void main(String[] args) throws Exception {
        System.out.println("Starting at hour: " + System.currentTimeMillis());

        int batchSize = 10;
        int vectorSize = 300;
        int nEpochs = 1;
        int truncateReviewsToLength = 55;
/*
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                .updater(Updater.ADAM).adamMeanDecay(0.9).adamVarDecay(0.999)
                .weightInit(WeightInit.XAVIER)
                .learningRate(0.005)
                .list()
                .layer(0, new GravesBidirectionalLSTM.Builder().nIn(vectorSize).nOut(256)
                        .activation(Activation.TANH).build())
                .layer(1, new GravesBidirectionalLSTM.Builder().nIn(256).nOut(256)
                        .activation(Activation.TANH).build())
                .layer(2, new RnnOutputLayer.Builder().activation(Activation.SOFTMAX)
                        .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(256).nOut(3).build())
                .pretrain(false).backprop(true).build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();

        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(WORD_VECTORS_PATH));
        AspectIterator train = new AspectIterator(DATA_PATH, wordVectors, batchSize, truncateReviewsToLength, true);
        AspectIterator test = new AspectIterator(DATA_PATH, wordVectors, batchSize, truncateReviewsToLength, false);

        System.out.println("Starting training");
        for (int i = 0; i < nEpochs; i++) {
            net.fit(train);
            train.reset();
            System.out.println("Epoch " + i + " complete. Starting evaluation:");

            Evaluation evaluation = new Evaluation();
            while (test.hasNext()) {
                DataSet t = test.next();
                INDArray features = t.getFeatureMatrix();
                INDArray lables = t.getLabels();
                INDArray inMask = t.getFeaturesMaskArray();
                INDArray outMask = t.getLabelsMaskArray();
                INDArray predicted = net.output(features, false, inMask, outMask);

                evaluation.evalTimeSeries(lables, predicted, outMask);
            }
            test.reset();

            System.out.println(evaluation.stats());

        }

        File locationToSave = new File("src/main/resources/aspectsNetwork3.net");
        boolean saveUpdater = true;
        ModelSerializer.writeModel(net, locationToSave, true);
*/

        long startTime = System.currentTimeMillis();
        net = ModelSerializer.restoreMultiLayerNetwork("src/main/resources/aspectsNetwork3.net");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter sentence:");
        String review=(scanner.nextLine());
/*
        INDArray features = loadFeaturesFromString(review, 10);
        INDArray networkOutput = net.output(features);
        int timeSeriesLength = networkOutput.size(2);
        INDArray probabilitiesForLabels = networkOutput.get(NDArrayIndex.point(0), NDArrayIndex.all(), NDArrayIndex.point(timeSeriesLength - 1));

        System.out.println("\n\n-------------------------------");
        System.out.println("Analyizing the review: \n" + review);
        System.out.println("\n\nProbabilities for labels:");
        System.out.println("food: " + probabilitiesForLabels.getDouble(0));
        System.out.println("drinks: " + probabilitiesForLabels.getDouble(1));
        System.out.println("service: " + probabilitiesForLabels.getDouble(2));
*/


        File categories = new File("src/main/resources/categories.txt");
        DataSet testNews = prepareTestData(review);
        INDArray fet = testNews.getFeatureMatrix();
        INDArray predicted = net.output(fet, false);
        int arrsiz[] = predicted.shape();

        double max = 0;
        int pos = 0;
        for (int i = 0; i < arrsiz[1]; i++) {
            System.out.println((double) predicted.getColumn(i).sumNumber());
            if (max < (double) predicted.getColumn(i).sumNumber()) {
                max = (double) predicted.getColumn(i).sumNumber();
                pos = i;
            }
        }

        try (BufferedReader brCategories = new BufferedReader(new FileReader(categories))) {
            String temp = "";
            List<String> labels = new ArrayList<>();
            while ((temp = brCategories.readLine()) != null) {
                labels.add(temp);
            }
            brCategories.close();
            System.out.println("The aspect of the sentence: " + labels.get(pos).split(",")[1]);
        } catch (Exception e) {
            System.out.println("File Exception : " + e.getMessage());
        }

       // System.out.println("The aspect of the sentence " + pos);


        /*
        System.out.println("restaurant miscellaneous: " + probabilitiesForLabels.getDouble(4));
        System.out.println("food general: " + probabilitiesForLabels.getDouble(5));
        System.out.println("food prices: " + probabilitiesForLabels.getDouble(6));
        System.out.println("food quality: " + probabilitiesForLabels.getDouble(7));
        System.out.println("food style_options: " + probabilitiesForLabels.getDouble(8));
        System.out.println("food miscellaneous: " + probabilitiesForLabels.getDouble(9));
        System.out.println("drinks general: " + probabilitiesForLabels.getDouble(10));
        System.out.println("drinks prices: " + probabilitiesForLabels.getDouble(11));
        System.out.println("drinks quality: " + probabilitiesForLabels.getDouble(12));
        System.out.println("drinks style_options: " + probabilitiesForLabels.getDouble(13));
        System.out.println("drinks miscellaneous: " + probabilitiesForLabels.getDouble(14));
        System.out.println("ambience general: " + probabilitiesForLabels.getDouble(15));
        System.out.println("service general: " + probabilitiesForLabels.getDouble(16));
        System.out.println("location general: " + probabilitiesForLabels.getDouble(17));*/



        System.out.println("----- Example complete -----");
        long endTime = System.currentTimeMillis();
        System.out.println("Execution took " + TimeUnit.MILLISECONDS.toMinutes(endTime-startTime) + " minutes");



    }

    public static INDArray loadFeaturesFromString(String reviewContents, int maxLength) throws FileNotFoundException, UnsupportedEncodingException {
        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(WORD_VECTORS_PATH));

        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        List<String> tokens = tokenizerFactory.create(reviewContents).getTokens();
        List<String> tokensFiltered = new ArrayList<>();
        for(String t : tokens ){
            if(wordVectors.hasWord(t)) tokensFiltered.add(t);
        }
        int outputLength = Math.max(maxLength,tokensFiltered.size());

        INDArray features = Nd4j.create(1, 300, outputLength);

        for( int j=0; j<tokens.size() && j<maxLength; j++ ){
            String token = tokens.get(j);
            INDArray vector = wordVectors.getWordVectorMatrix(token);
            features.put(new INDArrayIndex[]{NDArrayIndex.point(0), NDArrayIndex.all(), NDArrayIndex.point(j)}, vector);
        }
        return features;
    }





    private static DataSet prepareTestData(String i_news) {
        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(WORD_VECTORS_PATH));
        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        List<String> news = new ArrayList<>(1);
        int[] category = new int[1];
        int currCategory = 0;
        news.add(i_news);

        List<List<String>> allTokens = new ArrayList<>(news.size());
        int maxLength = 0;
        for (String s : news) {
            List<String> tokens = tokenizerFactory.create(s).getTokens();
            List<String> tokensFiltered = new ArrayList<>();
            for (String t : tokens) {
                if (wordVectors.hasWord(t)) tokensFiltered.add(t);
            }
            allTokens.add(tokensFiltered);
            maxLength = Math.max(maxLength, tokensFiltered.size());
        }

        INDArray features = Nd4j.create(news.size(), 300, maxLength);
        INDArray labels = Nd4j.create(news.size(), 3, maxLength);
        INDArray featuresMask = Nd4j.zeros(news.size(), maxLength);
        INDArray labelsMask = Nd4j.zeros(news.size(), maxLength);

        int[] temp = new int[2];
        for (int i = 0; i < news.size(); i++) {
            List<String> tokens = allTokens.get(i);
            temp[0] = i;
            for (int j = 0; j < tokens.size() && j < maxLength; j++) {
                String token = tokens.get(j);
                INDArray vector = wordVectors.getWordVectorMatrix(token);
                features.put(new INDArrayIndex[]{NDArrayIndex.point(i),
                                NDArrayIndex.all(),
                                NDArrayIndex.point(j)},
                        vector);

                temp[1] = j;
                featuresMask.putScalar(temp, 1.0);
            }
            int idx = category[i];
            int lastIdx = Math.min(tokens.size(), maxLength);
            labels.putScalar(new int[]{i, idx, lastIdx - 1}, 1.0);
            labelsMask.putScalar(new int[]{i, lastIdx - 1}, 1.0);
        }

        DataSet ds = new DataSet(features, labels, featuresMask, labelsMask);
        return ds;
    }
}


