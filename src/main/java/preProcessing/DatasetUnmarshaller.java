package preProcessing;

import generated.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Asus on 7/1/2017.
 */
public class DatasetUnmarshaller {

    private static int positiveReviews = 0;
    private static int negativeReviews = 0;
    private static int neutralReviews = 0;
    private static int numberOfReviews = 0;
    private static int maxNumberOfSentences = 0;
    private static int maxNumberOfTokens = 0;

    private String pathToSaveData;
    private File xmlFileToBeParsed;
    private HashMap<String, Integer> aspectMap;


    public DatasetUnmarshaller(String pathToSaveData, File xmlFileToBeParsed) {
        this.pathToSaveData = pathToSaveData;
        this.xmlFileToBeParsed = xmlFileToBeParsed;
        this.positiveReviews = 0;
        this.negativeReviews = 0;
        createAspectMap();
    }

    private void createAspectMap() {
        this.aspectMap = new HashMap<>();
        /*
        this.aspectMap.put("restaurant general", 0);
        this.aspectMap.put("restaurant prices", 0);
        this.aspectMap.put("restaurant quality", 0);
        this.aspectMap.put("restaurant style_options", 0);
        this.aspectMap.put("restaurant miscellaneous", 0);

        this.aspectMap.put("food general", 0);
        this.aspectMap.put("food prices", 0);
        this.aspectMap.put("food quality", 0);
        this.aspectMap.put("food style_options", 0);
        this.aspectMap.put("food miscellaneous", 0);

        this.aspectMap.put("drinks general", 0);
        this.aspectMap.put("drinks prices", 0);
        this.aspectMap.put("drinks quality", 0);
        this.aspectMap.put("drinks style_options", 0);
        this.aspectMap.put("drinks miscellaneous", 0);

        this.aspectMap.put("ambience general", 0);
        this.aspectMap.put("service general", 0);
        this.aspectMap.put("location general", 0);*/

        /*
        this.aspectMap.put("restaurant general", 0);
        this.aspectMap.put("food quality", 1);
        this.aspectMap.put("ambience general", 2);
        this.aspectMap.put("service general", 3);*/

        this.aspectMap.put("food", 0);
        this.aspectMap.put("drinks", 1);
        this.aspectMap.put("service", 2);



    }


    @SuppressWarnings("unchecked")
    public void unmarshallXML() {
        JAXBContext jaxbContextc;
        try {
            jaxbContextc = JAXBContext.newInstance("generated");
            javax.xml.bind.Unmarshaller um = jaxbContextc.createUnmarshaller();
            JAXBElement<ReviewsType> root = (JAXBElement<ReviewsType>) um.unmarshal(xmlFileToBeParsed);
            ReviewsType reviews = root.getValue();
            List<ReviewType> listOfReviews =reviews.getReview();
            numberOfReviews = listOfReviews.size();
            int index = 0;
            for (int i = 0; i < numberOfReviews; i++) {

                SentencesType sentencesForTheReview = listOfReviews.get(i).getSentences();
                List<SentenceType> sentences = sentencesForTheReview.getSentence();
                for (int j = 0 ; j < sentences.size(); j ++) {
                    List<String> enitities = new ArrayList<>();
                    OpinionsType opinionsType = sentences.get(j).getOpinions();
                    if (opinionsType != null) {
                        List<OpinionType> opinions = opinionsType.getOpinion();
                        for (int k = 0; k < opinions.size(); k ++) {
                            String polarity = opinions.get(k).getPolarity();
                            //if ("neutral".equals(polarity))
                              //  continue;
                            String aspect = opinions.get(k).getCategory().toLowerCase();
                            String[] aspectParts = aspect.split("#");
                            String entity = aspectParts[0];
                            if ("ambience".equals(entity) || "location".equals(entity)) {
                                entity = "drinks";
                            }
                            String attribute = aspectParts[1];
                            StringBuilder sb = new StringBuilder();
                            sb.append(entity);
                            sb.append(" ");
                            sb.append(attribute);
                            enitities.add(entity);
                            for (Map.Entry<String, Integer> aspectEntry : aspectMap.entrySet()) {
                                String checker = aspectEntry.getKey();
                                if (checker.equals(entity)) {
                                    int o = aspectEntry.getValue();
                                    o++;
                                    aspectEntry.setValue(o);
                                    FileWriter writer = new FileWriter(pathToSaveData+ "aspects/" + String.valueOf(index) + ".txt");
                                    writer.write(sentences.get(j).getText());
                                    writer.write(" ASPECT: ");
                                    writer.write(entity);
                                    writer.write("\n");
                                    writer.close();
                                    index++;
                                    checkPolarity(sentences.get(j).getText(), polarity, pathToSaveData, entity);
                                }
                            }
                            //checkPolarity(sentences.get(j).getText(), polarity, pathToSaveData, entity, attribute);
                            /*
                            FileWriter writer = new FileWriter(pathToSaveData+ String.valueOf(index) + ".txt");
                            writer.write(sentences.get(j).getText());
                            writer.write(" ASPECT: ");
                            writer.write(entity + " ");
                            writer.write(attribute);
                            writer.write("\n");
                            writer.close();
                            index++;*/
                        }
                        calculateNumberOfTokens(sentences, j);
                    }
                }

                maxNumberOfSentences = Math.max(sentences.size(), maxNumberOfSentences);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printDatasetAnalysis() {
        System.out.println(numberOfReviews);
        System.out.println(maxNumberOfSentences);
        System.out.println(maxNumberOfTokens);
        System.out.println(positiveReviews);
        System.out.println(neutralReviews);
        System.out.println(negativeReviews);
        System.out.println(positiveReviews + negativeReviews);
        System.out.println("Ga");
        int totalAspects = 0;
        for (Map.Entry<String, Integer> aspectEntry : aspectMap.entrySet()) {
            totalAspects = totalAspects + aspectEntry.getValue();
            //if (aspectEntry.getValue() == 0 )
            System.out.println(aspectEntry.getKey() + "    " + aspectEntry.getValue());
        }
        System.out.println("Total number of aspects " + totalAspects);

    }


    private static void checkPolarity(String sentence, String polarity, String pathToSaveData, String entity) throws IOException {
        switch(polarity){
            case "positive":
                positiveReviews ++;
                writeToFile(sentence, "positives", positiveReviews, pathToSaveData, entity);
                break;
            case "negative":
                negativeReviews ++;
                writeToFile(sentence, "negatives", negativeReviews, pathToSaveData, entity);
                break;
            default:
                break;
        }
    }


    private static void calculateNumberOfTokens(List<SentenceType> sentences, int j) {
        StringTokenizer st = new StringTokenizer(sentences.get(j).getText());
        int numberOfTokens = st.countTokens();
        maxNumberOfTokens = Math.max(numberOfTokens, maxNumberOfTokens);
    }


    private static void writeToFile(String sentence, String folder, int id, String pathToSaveData, String entity) throws IOException {
        FileWriter writer = new FileWriter(pathToSaveData + folder +"/"+ String.valueOf(id) + ".txt");
        writer.write(sentence);
        writer.write(" ASPECT: ");
        writer.write(entity);
        writer.write("\n");
        writer.close();
    }


}
