package preProcessing;

import java.io.File;

/**
 * Created by Asus on 7/1/2017.
 */
public class DatasetParser {

    //private static final String PATH_TEST = "C:/Folder/Lic/Data/DataWithAspects/Test/";
    //private static final String PATH_TRAIN = "C:/Folder/Lic/Data/DataWithAspects/Train/";
    private static final String PATH_TEST = "src/main/resources/DataWithAspects/Test/";
    private static final String PATH_TRAIN = "src/main/resources/DataWithAspects/Train/";
    private static final File XMLFILE_TEST = new File("src/main/resources/ABSA15_Restaurants_Test.xml");
    private static final File XMLFILE_TRAIN = new File("src/main/resources/ABSA-15_Restaurants_Train_Final.xml");


    public static void main(String[] args) {
        DatasetUnmarshaller trainDatasetUnmarshaller = new DatasetUnmarshaller(PATH_TRAIN, XMLFILE_TRAIN);
        trainDatasetUnmarshaller.unmarshallXML();
        trainDatasetUnmarshaller.printDatasetAnalysis();

        DatasetUnmarshaller testDatasetUnmarshaller = new DatasetUnmarshaller(PATH_TEST, XMLFILE_TEST);
        testDatasetUnmarshaller.unmarshallXML();
       testDatasetUnmarshaller.printDatasetAnalysis();
    }
}
